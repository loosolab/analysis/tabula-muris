# tabula-muris

Tabula Muris is a compendium of single cell transcriptome data from the model organism Mus musculus, containing nearly 100,000 cells from 20 organs and tissues. This repository contains the *Loosolab-specific* analysis thereof.