#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pickle
import numpy as np
import pandas as pd
import scanpy as sc
import matplotlib.pyplot as plt
import scvelo as scv

sc.settings.verbosity = 3
sc.set_figure_params(color_map='viridis', dpi_save=150)
sc.logging.print_versions()


# # Tabula muris data processing

# We first load the raw counts and metadata ..

# In[2]:


from glob import glob

## Read in each tissue-specific matrix and concatenate them
counts = [pd.read_csv(file, index_col=0).T for file in glob('../raw/*-counts.csv')]
counts = pd.concat(counts)


# In[3]:


## Load the metadata
annotations = pd.read_csv('../raw/annotations_facs.csv', index_col='cell')

## Pull the annotation fields we want
annotation_fields = ['cell_ontology_class', 'cluster.ids', 'free_annotation', 'mouse.sex', 'mouse.id', 
                     'plate.barcode', 'tissue', 'subtissue']

## Find annotated cells only
annotated_cells = list(counts.index.intersection(annotations.index))


# In[4]:


adata = sc.AnnData(X=counts.loc[annotated_cells, ])
adata.obs = adata.obs.join(annotations[annotation_fields])
adata.obs['cluster.ids'] = adata.obs['cluster.ids'].astype('str')


# We next add the tissue-specific t-SNE dimension reduction from Tabula Muris

# In[5]:


tsne_fields = ['tissue_tSNE_1', 'tissue_tSNE_2']
annotations = annotations.reindex(adata.obs.index)
adata.obsm['X_tissue_tsne'] = annotations[tsne_fields].to_numpy()


# In[6]:


(cell_qc_data, gene_qc_data)= sc.pp.calculate_qc_metrics(adata)


# ## Cell and gene filtering

# We start by filtering out cells that show expression in less than 500 cells. We also exclude cells with less than 50,000 reads or UMI counts 

# In[7]:


sc.pp.filter_cells(adata, min_genes=500)
sc.pp.filter_cells(adata, min_counts=50000)


# ## Expression normalisation

# In[8]:


sc.pp.normalize_per_cell(adata, counts_per_cell_after=1000000)


# In[9]:


sc.pp.log1p(adata)


# ## Highly variable genes

# In[10]:


sc.pp.highly_variable_genes(adata, min_disp=0.5)


# ## Principal component analysis

# In[11]:


sc.pp.pca(adata, use_highly_variable = True)


# In[12]:


sc.pl.pca_variance_ratio(adata)


# In[13]:


sc.pp.neighbors(adata, n_pcs=13, metric='euclidean', use_rep='X_pca')


# ## Lower dimensional embeddings

# ### t-SNE embedding

# In[14]:


sc.tl.tsne(adata, perplexity=30, n_pcs = 13, use_rep='X_pca')


# ### UMAP embedding

# In[15]:


sc.tl.umap(adata, min_dist = 0.1, spread = 2)


# ### Cell clustering

# In[16]:


sc.tl.leiden(adata)
sc.tl.louvain(adata)


# ## Postprocessing

# In[17]:


adata.obs.rename(columns={'cluster.ids': 'clusters_from_manuscript',
                          'mouse.sex': 'mouse_sex', 
                          'mouse.id': 'mouse_id', 
                          'plate.barcode': 'plate_barcode', 
                          'louvain': 'clusters_louvain', 
                          'leiden': 'clusters_leiden'}, inplace=True)


# In[18]:


adata.write('../data/tabula-muris.h5ad')

